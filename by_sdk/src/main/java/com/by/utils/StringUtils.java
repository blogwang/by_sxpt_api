package com.by.utils;

/**
 * @author soberw
 * @Classname StringUtils
 * @Description
 * @Date 2022-03-18 15:21
 */
public class StringUtils {

    /**
     * 判断字符串是否为空串（null  ""  "  "）
     * @param s
     * @return
     */
    public static boolean isBlank(String s){
        if(null == s){
            return true;
        }
        if(s.trim().length()==0){
            return true;
        }
        return false;
    }

    /**
     * 判断字符串是否不为空串（null  ""  "  "）
     * @param s
     * @return
     */
    public static boolean isNotBlank(String s){
        return isBlank(s);
    }
}
