package com.by.utils;

import net.coobird.thumbnailator.Thumbnails;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

/**
 * @author soberw
 * @Classname UploadUtils
 * @Description
 * @Date 2022-03-21 16:54
 */
public class UploadUtils {

    //附件上传的服务器路径
    private final static String ATTACHMENT_DIRS = "/attachment/";

    /**
     * 上传图片并压缩到指定大小
     *
     * @param multipartFile
     * @param rootPath
     * @param width
     * @param height
     * @return 上传后在服务器的根路径
     */
    public static String uploadPic(MultipartFile multipartFile, String dirType, String rootPath, int width, int height) {
        //创建服务器目录
        String path = createDir(rootPath, dirType);
        //图片上传后的路径
        String tmp = writeFile(multipartFile, rootPath, path);
        //产生缩略图
        try {
            Thumbnails.of(rootPath + tmp)
                    .size(width, height)
                    .toFile(rootPath + tmp);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return tmp;
    }

    /**
     * 在原有图片路径上  加上  .mini.
     *
     * @param path
     * @return
     */
    static String getMiniFile(String path) {
        //获取后缀
        String ext = path.substring(path.lastIndexOf("."));
        //加上新的后缀
        String mini = path.replace("ext", ".mini" + ext);
        return mini;
    }

    /**
     * 将文件 写入服务器
     *
     * @param file
     * @return
     */
    static String writeFile(MultipartFile file, String rootPath, String relativePath) {
        if (file.isEmpty()) {
            return "上传的文件为空";
        }

        //获取新文件名
        String fileName = getNewFileName(file);
        //创建新的文件
        File newFile = new File(rootPath + relativePath + fileName);
        //将文件写入服务器
        try {
            file.transferTo(newFile);
            return relativePath + fileName;
        } catch (IOException e) {
            e.printStackTrace();
            return "上传失败";
        }
    }

    /**
     * 生成新的文件名
     *
     * @param file
     * @return
     */
    static String getNewFileName(MultipartFile file) {
        //获取原文件的后缀
        String name = file.getOriginalFilename();
        String ext = name.substring(name.lastIndexOf("."));
        String newFileName = System.currentTimeMillis() + ext;
        return newFileName;
    }


    /**
     * 创建附件在服务器存储的目录
     *
     * @param rootPath 服务器根路径
     * @param dirType  附件类别 如 pic
     * @return 服务器存储地址
     */
    private static String createDir(String rootPath, String dirType) {
        //获取当前系统日期
        String time = DateUtils.getYmd();
        //附件存储路径
        String tmp = ATTACHMENT_DIRS + dirType + "/" + time + "/";
        //创建服务器附件的路径
        File dir = new File(rootPath + tmp);
        //先判断目录 是否存在
        if (!dir.exists()) {
            //创建目录
            dir.mkdirs();
        }
        return tmp;
    }
}
