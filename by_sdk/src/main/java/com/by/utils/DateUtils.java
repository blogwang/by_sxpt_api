package com.by.utils;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * @author soberw
 * @Classname DateUtils
 * @Description
 * @Date 2022-03-21 17:00
 */
public class DateUtils {
    public static String getYmd(){
        LocalDate localDate = LocalDate.now();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return localDate.format(formatter);
    }
}
