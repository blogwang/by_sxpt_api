package com.by.dao.sys;

import com.by.bean.User;
import com.by.common.business.IDao;
import org.apache.ibatis.annotations.Mapper;

import java.util.Map;

/**
 * @author soberw
 */
@Mapper
public interface UserDao extends IDao<User> {

    int checkAccount(String account);

    int changeStatus(Map map);

    int resetpwd(String id);
}
