package com.by.dao.sys;

import com.by.bean.Dict;
import com.by.common.business.IDao;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author soberw
 * @Classname DictDao
 * @Description
 * @Date 2022-03-15 21:09
 */
@Mapper
public interface DictDao extends IDao<Dict> {
    /**
     * 查询指定字典下的列表项
     * @return
     */
    List queryChild(Map map);

    /**
     * 根据parentID删除对应的字典项
     * @param pid
     * @return
     */
    int deleteByParentId(String pid);

    int checkCode(String code);
}
