package com.by.dao.sys;

import com.by.bean.Role;
import com.by.common.business.IDao;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author soberw
 * @Classname RoleDao
 * @Description
 * @Date 2022-03-23 19:01
 */
@Mapper
public interface RoleDao extends IDao<Role> {
    List<Map> queryAll();
}
