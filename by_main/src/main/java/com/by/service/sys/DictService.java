package com.by.service.sys;

import com.by.bean.Dict;
import com.by.common.business.IService;

import java.util.List;

/**
 * @author soberw
 * @Classname DictService
 * @Description
 * @Date 2022-03-15 21:06
 */
public interface DictService extends IService<Dict>{

    /**
     * 查询所有分组
     * @return
     */
    List queryGroup();

    /**
     * 根据parentID删除对应的字典项
     * @param pid
     * @return
     */
    int deleteByParentId(String pid);


    /**
     * 查询是否有已经存在的编码
     * @param code
     * @return
     */
    int checkCode(String code);

}
