package com.by.service.sys.impl;

import com.by.bean.Role;
import com.by.common.business.BaseServiceImpl;
import com.by.dao.sys.RoleDao;
import com.by.service.sys.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * @author soberw
 * @Classname RoleServiceImpl
 * @Description
 * @Date 2022-03-23 19:02
 */
@Service
public class RoleServiceImpl extends BaseServiceImpl<Role, RoleDao> implements RoleService {
    @Override
    public List<Map> queryAll() {
        return baseDao.queryAll();
    }
}
