package com.by.service.sys.impl;

import com.by.bean.User;
import com.by.common.business.BaseServiceImpl;
import com.by.dao.sys.UserDao;
import com.by.service.sys.UserService;
import org.springframework.stereotype.Service;

import java.util.Map;

/**
 * @author soberw
 */
@Service
public class UserServiceImpl  extends BaseServiceImpl<User,UserDao> implements UserService {


    @Override
    public int checkAccount(String account) {
        return baseDao.checkAccount(account);
    }

    @Override
    public int changeStatus(Map map) {
        return baseDao.changeStatus(map);
    }

    @Override
    public int resetpwd(String id) {
        return baseDao.resetpwd(id);
    }
}
