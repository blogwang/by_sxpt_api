package com.by.service.sys;

import com.by.bean.User;
import com.by.common.business.IService;

import java.util.Map;

/**
 * @author soberw
 */
public interface UserService extends IService<User> {

    /**
     * 检查账号是否唯一存在
     * @param account  账号
     * @return  0 不存在 不为0说明存在
     */
    int checkAccount(String account);

    /**
     * 更改用户状态值
     * @param map  条件
     * @return  0  更改失败   1更改成功
     */
    int changeStatus(Map map);

    /**
     * 重置指定用户的密码
     * @param id
     * @return
     */
    int resetpwd(String id);
}
