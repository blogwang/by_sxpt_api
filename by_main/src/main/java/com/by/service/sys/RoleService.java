package com.by.service.sys;

import com.by.bean.Role;
import com.by.common.business.IService;

import java.util.List;
import java.util.Map;

/**
 * @author soberw
 * @Classname RoleService
 * @Description
 * @Date 2022-03-23 19:02
 */
public interface RoleService extends IService<Role> {
    /**
     * 查询所有角色
     * @return
     */
    List<Map> queryAll ();
}
