package com.by.service.sys.impl;

import com.by.bean.Dict;
import com.by.common.business.BaseServiceImpl;
import com.by.dao.sys.DictDao;
import com.by.service.sys.DictService;
import com.by.utils.StringUtils;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author soberw
 * @Classname DictServiceImpl
 * @Description
 * @Date 2022-03-15 21:08
 */
@Service
public class DictServiceImpl extends BaseServiceImpl<Dict, DictDao> implements DictService {
    @Override
    public int save(Dict dict) {
        return super.save(dict);
    }

    @Override
    public List queryGroup() {
        Map map = new HashMap();
        map.put("type",1);
        return baseDao.queryChild(map);
    }

    @Override
    public int deleteByParentId(String pid) {
        return baseDao.deleteByParentId(pid);
    }

    @Override
    public int checkCode(String code) {
        return baseDao.checkCode(code);
    }
}
