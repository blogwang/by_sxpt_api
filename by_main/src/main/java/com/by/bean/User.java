package com.by.bean;

import com.by.group.SaveValidate;
import com.by.group.UpdateValidate;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.*;
import java.sql.Savepoint;

@Data
public class User {

    @NotBlank(message = "用户id不能为空",groups = {UpdateValidate.class})
    private String id;

    @NotBlank(message = "账号不能为空",groups = {SaveValidate.class})
    private String account;

    @NotBlank(message = "密码不能为空",groups = {SaveValidate.class})
    private String password;

    @NotBlank(message = "确认密码不能为空",groups = {SaveValidate.class})
    private String confirmpwd;

    private String name;

    private Integer accountType;

    private String infoId;

    @NotBlank(message = "创建人不能为空",groups = {SaveValidate.class})
    private String createUser;

    @NotBlank(message = "更新人不能为空",groups = {UpdateValidate.class})
    private String updateUser;

    private String headerimg;

    private String roleId;

}
