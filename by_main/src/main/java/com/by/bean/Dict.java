package com.by.bean;

import com.by.group.SaveValidate;
import com.by.group.UpdateValidate;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author soberw
 * @Classname Dict
 * @Description
 * @Date 2022-03-15 20:58
 */
@Data
public class Dict {

    @NotBlank(message = "id不能为空",groups = {UpdateValidate.class})
    private String id;

    @NotNull(message = "字典类型不能为空",groups = {SaveValidate.class})
    @Min(value = 1,message = "字典类型最小是1",groups = {SaveValidate.class})
    @Max(value = 2,message = "字典类型最大是2",groups = {SaveValidate.class})
    private String type;

    @NotBlank(message = "字典编码不能为空",groups = {SaveValidate.class})
    private String dictCode;
    @NotBlank(message = "字典名称不能为空！",groups = {SaveValidate.class,UpdateValidate.class})
    private String dictName;
    private String note;
    private Integer sorts;
    private String parentId;
}
