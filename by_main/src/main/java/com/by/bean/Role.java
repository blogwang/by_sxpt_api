package com.by.bean;

import lombok.Data;

/**
 * @author soberw
 * @Classname Role
 * @Description
 * @Date 2022-03-23 18:59
 */
@Data
public class Role {
    private String id;
    private String roleName;
    private String roleDesc;
    private String auths;
    private String createUser;
    private String updateUser;
}
