package com.by.controller.sys;

import com.by.bean.User;
import com.by.common.bean.PageBean;
import com.by.common.bean.ResultBean;
import com.by.common.controller.BaseController;
import com.by.group.SaveValidate;
import com.by.group.UpdateValidate;
import com.by.service.sys.UserService;
import com.by.utils.StringUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

/**
 * @author soberw
 */
@RestController
@RequestMapping("/api/user")
public class UserController extends BaseController {

    @Autowired
    UserService userService;

    @PostMapping("/save")
    @ApiOperation(value = "保存用户")
    public ResultBean save(@RequestBody @Validated(SaveValidate.class) User user, BindingResult result) {


        Map rs = validate(result);
        if (rs.size() > 0) {
            return validatefail(rs);
        }

        //检验账号是否唯一
        int i = userService.checkAccount(user.getAccount());
        if (i >= 1) {
            return fail("此账号已经存在！");
        }
        //检验两次密码是否一致
        if (!user.getPassword().equals(user.getConfirmpwd())) {
            return fail("两次密码不一致！");
        }

        int tmp = userService.save(user);

        return tmp > 0 ? success(Message.SAVE_SUCCESS) : fail(Message.SAVE_FAIL);
    }


    @PostMapping("/update")
    @ApiOperation(value = "修改用户")
    public ResultBean update(@RequestBody @Validated(UpdateValidate.class) User user, BindingResult result) {

        Map rs = validate(result);
        if (rs.size() > 0) {
            return validatefail(rs);
        }
        int update = userService.update(user);

        return update > 0 ? success("修改成功") : fail("修改失败");
    }

    @PostMapping("/query")
    public ResultBean query(@RequestBody Map map) {
        PageBean bean = userService.query(map);
        return success("用户列表", bean);
    }

    @GetMapping("/check")
    public ResultBean checkAccount(String account) {
        if (StringUtils.isBlank(account)) {
            return fail("账号不能为空");
        }
        int rs = userService.checkAccount(account);
        return success("账号唯一检查", rs);
    }

    @DeleteMapping("/delete/{id}")
    public ResultBean delete(@PathVariable("id") String id) {
        int delete = userService.delete(id);
        return delete > 0 ? success("删除成功") : fail("删除失败");
    }

    @PostMapping("/changeStatus")
    public ResultBean changeUserStatus(@RequestBody Map map){
        int rs = userService.changeStatus(map);
        return rs > 0 ? success("更改成功") : fail("更改失败");
    }

    @GetMapping("/resetpwd")
    public ResultBean resetpwd(String id){
        int rs = userService.resetpwd(id);
        return rs > 0 ? success("重置成功！初始密码为：123456") : fail("重置失败");
    }


}
