package com.by.controller.sys;

import com.by.bean.Role;
import com.by.common.bean.PageBean;
import com.by.common.bean.ResultBean;
import com.by.common.controller.BaseController;
import com.by.service.sys.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author soberw
 * @Classname RoleController
 * @Description
 * @Date 2022-03-23 19:00
 */
@RestController
@RequestMapping("/api/role")
public class RoleController extends BaseController {
    @Autowired
    RoleService roleService;

    @PostMapping("/save")
    public ResultBean save(@RequestBody Role role) {
        int rs = roleService.save(role);
        return rs > 0 ? success("保存成功") : fail("保存失败");
    }

    @PutMapping("/update")
    public ResultBean update(@RequestBody Role role) {
        int rs = roleService.update(role);
        return rs > 0 ? success("保存成功") : fail("保存失败");
    }

    @DeleteMapping("/delete/{id}")
    public ResultBean delete(@PathVariable("id") String id) {
        int rs = roleService.delete(id);
        return rs > 0 ? success("删除成功") : fail("删除失败");
    }

    @PostMapping("/query")
    public ResultBean query(@RequestBody Map map) {
        PageBean bean = roleService.query(map);
        return success("角色列表", bean);
    }

    @GetMapping("/queryall")
    public ResultBean queryAll() {
        List list = roleService.queryAll();
        return success("角色列表", list);
    }


}
