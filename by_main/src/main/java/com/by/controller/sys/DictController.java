package com.by.controller.sys;

import com.by.bean.Dict;
import com.by.common.bean.PageBean;
import com.by.common.bean.ResultBean;
import com.by.common.controller.BaseController;
import com.by.group.SaveValidate;
import com.by.group.UpdateValidate;
import com.by.service.sys.DictService;
import com.by.utils.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author soberw
 * @Classname DictController
 * @Description
 * @Date 2022-03-15 20:55
 */
@RestController
@RequestMapping("/api")
public class DictController extends BaseController {

    @Autowired
    DictService dictService;

    @PostMapping("/dict")
    public ResultBean save(@Validated({SaveValidate.class}) @RequestBody Dict dict, BindingResult result) {
        Map errors = this.validate(result);
        if (errors.size() > 0) {
            return fail("验证失败", errors);
        }
        int save = dictService.checkCode(dict.getDictCode());
        if (save == 1) {
            return fail("此编码已经存在！");
        }
        save = dictService.save(dict);
        return save == 1 ? this.success("保存成功") : this.fail("保存失败");
    }

    @PostMapping("/dict/query")
    public ResultBean query(@RequestBody Map map) {
        PageBean bean = dictService.query(map);
        return success("字典列表", bean);
    }

    @PutMapping("/dict")
    public ResultBean update(@Validated({UpdateValidate.class}) @RequestBody Dict dict, BindingResult result) {
        Map errors = this.validate(result);
        if (errors.size() > 0) {
            return fail("验证失败", errors);
        }
        int i = dictService.update(dict);
        return i == 1 ? this.success("更新成功") : this.fail("更新失败");
    }

    @DeleteMapping("/dict/{id}")
    public ResultBean delete(@PathVariable("id") String id) {
        int i = dictService.delete(id);
        return i > 0 ? this.success("删除成功") : this.fail("删除失败");
    }

    @DeleteMapping("/dictByParentId/{pid}")
    public ResultBean deleteByParentId(@PathVariable("pid") String pid) {
        int i = dictService.deleteByParentId(pid);
        return i > 0 ? this.success("删除成功") : this.fail("删除失败");
    }

    @GetMapping("/dict/group")
    public ResultBean queryGroup() {
        List queryGroup = dictService.queryGroup();
        return success("分组列表", queryGroup);
    }

    @GetMapping("/dict/checkCode")
    public ResultBean checkCode(String code) {
        if (StringUtils.isBlank(code)) {
            return fail("编码不能为空！");
        }
        int rs = dictService.checkCode(code);
        return success(rs);
    }


}
