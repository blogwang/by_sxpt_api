package com.by.controller.common;

import com.by.common.bean.ResultBean;
import com.by.common.controller.BaseController;
import com.by.utils.UploadUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;

/**
 * @author soberw
 * @Classname UploadController
 * @Description
 * @Date 2022-03-21 16:47
 */
@RestController
@RequestMapping("/upload")
public class UploadController extends BaseController {

    //服务器根路径
    private String root;

    @Autowired
    HttpServletRequest req;

    @PostMapping("/headerimg")
    public ResultBean uploadHeaderImg(MultipartFile file) {
        String rs = UploadUtils.uploadPic(file, "headerImg", getRoot(), 120, 120);

        return rs.contains("/") ? success("上传成功", rs) : fail(rs);
    }


    /**
     * 返回服务器根路径
     *
     * @return
     * @author soberw
     */
    public String getRoot() {
        if (null == root && null != req) {
            root = req.getSession().getServletContext().getRealPath("/");
        }
        return root;
    }
}
