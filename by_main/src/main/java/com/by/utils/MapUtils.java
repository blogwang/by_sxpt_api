package com.by.utils;

import java.util.Map;

public class MapUtils {

    public static int getIntValue(Map map, String key){
        if(null==map||null==key)
            return 0;
        if(!map.containsKey(key))
            return 0;
        Object obj=map.get(key);
        if(null==obj)
            return 0;
        return Integer.parseInt(obj.toString());
    }
}
