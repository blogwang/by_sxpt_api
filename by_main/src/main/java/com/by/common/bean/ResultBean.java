package com.by.common.bean;

import lombok.Data;

import java.io.Serializable;

@Data
public class ResultBean implements Serializable {

    int code ;//  请求状态码
    String msg;//请求消息描述
    Object data; //请求结果

    public ResultBean(int code, String msg, Object data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    public ResultBean() {
    }
}
