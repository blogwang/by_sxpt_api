package com.by.common.bean;

import java.util.HashMap;
import java.util.Map;

public class PageMap extends HashMap {

    private final String PAGENUM="pageNum";
    private final String PAGESIZE="pageSize";

    int pageNum;
    int pageSize;

    public PageMap(Map map){
        //将传递来的参数加入pageMap里
        this.putAll(map);
        setPageNum(getIntValue(PAGENUM,1));
        put(PAGENUM,getPageNum());
        setPageSize(getIntValue(PAGESIZE,10));
        put(PAGESIZE,getPageSize());
    }

    public PageMap(){}

    /**
     * 取出当前map中的值
     * @param key  map中的key
     * @param defaultVal  默认值
     * @return
     */
     int getIntValue(String key,int defaultVal){
        if(!containsKey(key))
            return defaultVal;
        Object obj=get(key);
        return null==obj?defaultVal: Integer.parseInt(obj.toString());
    }

    public int getPageNum() {
        return pageNum;
    }

    public void setPageNum(int pageNum) {
        this.pageNum = pageNum;
    }

    public int getPageSize() {
        return pageSize;
    }

    public void setPageSize(int pageSize) {
        this.pageSize = pageSize;
    }
}
