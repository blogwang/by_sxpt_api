package com.by.common.bean;

import com.github.pagehelper.Page;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author soberw
 */
@Data
public class PageBean implements Serializable {

    int pageNum;
    int pageSize;
    long total;
    int pages;
    Object list;


    public PageBean(List list) {
        this.list = list;
        Page page= (Page) list;
        this.pageNum = page.getPageNum();
        this.pageSize = page.getPageSize();
        this.total = page.getTotal();
        this.pages=page.getPages();
    }

    public PageBean() {
    }
}
