package com.by.common.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

@Configuration
@EnableOpenApi
public class SwaggerConfig {

    @Bean
    public Docket createDocket(){
        return new Docket(DocumentationType.SWAGGER_2)
                .apiInfo(getApiInfo())
                 .enable(true)//为true可以访问 false不能访问
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.by.controller"))
                .paths(PathSelectors.ant("/**"))  //  /** 代表所有接口生成文档
                .build();
    }

    private ApiInfo getApiInfo(){
        return new ApiInfoBuilder()
                .title("系统框架搭建")
                .description("快速系统框架搭建，方便系统开发")
                .contact(new Contact("牛肖宁", "http://localhost", "3519973006@qq.com"))
                .version("v1.0")
                .build();
    }

}
