package com.by.common.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.Properties;


@Configuration
@ConfigurationProperties(prefix = "spring.datasource")
@Data
public class DruidDataSourceConfig {

    private String url;
    private String username;
    private String password;
    private String driverClasName;



    /**
     * 创建数据源 放入ioc容器
     * @return
     */
    @Bean
    public DataSource dataSource(DruidPools druidPools) throws SQLException {
        DruidDataSource dataSource=new DruidDataSource();
        dataSource.setUrl(getUrl());
        dataSource.setUsername(getUsername());
        dataSource.setPassword(getPassword());
        dataSource.setDriverClassName(getDriverClasName());
        dataSource.setInitialSize(druidPools.getInitialSize());
        dataSource.setMinIdle(druidPools.getMinIdle());
        dataSource.setMaxActive(druidPools.getMaxActive());
        dataSource.setMaxWait(druidPools.getMaxWait());
        dataSource.setTimeBetweenEvictionRunsMillis(druidPools.getTimeBetweenEvictionRunsMillis());
        dataSource.setMinEvictableIdleTimeMillis(druidPools.getMinEvictableIdleTimeMillis());
        dataSource.setValidationQuery(druidPools.getValidationQuery());
        dataSource.setTestWhileIdle(druidPools.testWhileIdle);
        dataSource.setTestOnBorrow(druidPools.testOnBorrow);
        dataSource.setTestOnReturn(druidPools.testOnReturn);
        dataSource.setPoolPreparedStatements(druidPools.poolPreparedStatements);
        dataSource.setMaxPoolPreparedStatementPerConnectionSize(druidPools.maxPoolPreparedStatementPerConnectionSize);
        dataSource.setConnectionErrorRetryAttempts(druidPools.connectionErrorRetryAttempts);
        dataSource.setBreakAfterAcquireFailure(druidPools.breakAfterAcquireFailure);
        dataSource.setTimeBetweenConnectErrorMillis(druidPools.timeBetweenConnectErrorMillis);
        dataSource.setAsyncInit(druidPools.asyncInit);
        dataSource.setRemoveAbandoned(druidPools.removeAbandoned);
        dataSource.setRemoveAbandoned(druidPools.removeAbandoned);
        dataSource.setTransactionQueryTimeout(druidPools.transactionQueryTimeout);
        dataSource.setFilters(druidPools.filters);
        dataSource.setConnectProperties(druidPools.connectionProperties);
        return dataSource;
    }

    @Configuration
    @ConfigurationProperties(prefix = "spring.datasource.pools")
    @Data
    class DruidPools{
        int initialSize;
        int minIdle;
        int maxActive;
        int maxWait;
        long timeBetweenEvictionRunsMillis;
        long minEvictableIdleTimeMillis;
        String validationQuery;
        boolean testWhileIdle;
        boolean testOnBorrow;
        boolean testOnReturn;
        boolean poolPreparedStatements;
        int maxPoolPreparedStatementPerConnectionSize;
        int connectionErrorRetryAttempts;
        boolean breakAfterAcquireFailure;
        long timeBetweenConnectErrorMillis;
        boolean asyncInit;
        boolean removeAbandoned;
        long removeAbandonedTimeout;
        int transactionQueryTimeout;
        String filters;
        Properties connectionProperties;
    }

    /**
     * 配置servlet 查看线程池监控信息
     * @param statConfig
     * @return
     */
    @Bean
    public ServletRegistrationBean druidServlet(StatConfig statConfig){
        ServletRegistrationBean servletRegistrationBean = new ServletRegistrationBean(
                new StatViewServlet(), statConfig.getUrlPattern()); // 现在要进行druid监控的配置处理操作
        servletRegistrationBean.addInitParameter("allow", statConfig.getAllow()); // 白名单
         servletRegistrationBean.addInitParameter("deny", statConfig.getDeny()); // 黑名单
        servletRegistrationBean.addInitParameter("loginUsername", statConfig.getLoginUsername()); // 用户名
        servletRegistrationBean.addInitParameter("loginPassword", statConfig.getLoginPassword()); // 密码
        servletRegistrationBean.addInitParameter("resetEnable", statConfig.getResetEnable()); // 是否可以重置数据源
        return servletRegistrationBean ;
    }

    /**
     * 获取状态监控配置信息
     */
    @Configuration
    @ConfigurationProperties(prefix = "spring.datasource.stat-view-servlet")
    @Data
    class StatConfig{
       String  urlPattern;
       String allow;
       String deny;
       String resetEnable;
       String loginUsername;
       String loginPassword;
    }


    @Bean
    public FilterRegistrationBean filterRegistrationBean(StatFilter statFilter) {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean() ;
        filterRegistrationBean.setFilter(new WebStatFilter());
        filterRegistrationBean.addUrlPatterns(statFilter.urlPattern); // 所有请求进行监控处理
        filterRegistrationBean.addInitParameter("exclusions", statFilter.exclusions);
        filterRegistrationBean.setEnabled(statFilter.enabled);
        return filterRegistrationBean ;
    }
    @Configuration
    @ConfigurationProperties(prefix = "spring.datasource.web-stat-filter")
    @Data
    class StatFilter{
      boolean enabled;
      String  urlPattern;
      String exclusions;
    }

}
