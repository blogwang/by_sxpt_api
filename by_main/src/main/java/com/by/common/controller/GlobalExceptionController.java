package com.by.common.controller;


import com.by.common.bean.ResultBean;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.sql.SQLException;

@ControllerAdvice(basePackages = {"com.by"})
@ResponseBody
public class GlobalExceptionController extends BaseController {

    @ExceptionHandler(NullPointerException.class)
    public ResultBean processNullPointException(NullPointerException e) {
        String msg = e.toString();
        e.printStackTrace();
        return fail(msg);
    }

    @ExceptionHandler(SQLException.class)
    public ResultBean processSQLSyntaxErrorException(SQLException e) {
        e.printStackTrace();
        return fail(e.toString());
    }

}
