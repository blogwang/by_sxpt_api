package com.by.common.business;

import org.apache.ibatis.annotations.Mapper;

import java.util.List;
import java.util.Map;

/**
 * @author soberw
 */
@Mapper
public interface IDao<T> extends Crud<T>{

    List query(Map map);
}
