package com.by.common.business;

import com.by.common.bean.PageBean;
import com.by.common.bean.PageMap;
import com.by.dao.sys.UserDao;
import com.github.pagehelper.PageHelper;
import org.springframework.beans.factory.annotation.Autowired;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public abstract class BaseServiceImpl<T,D extends IDao > implements IService<T>{

    @Autowired(required = false)
    protected D baseDao;

    @Override
    public int save(T t) {
        return baseDao.save(t);
    }

    @Override
    public int update(T t) {
        return baseDao.update(t);
    }

    @Override
    public int delete(Serializable id) {
        return baseDao.delete(id);
    }

    @Override
    public T get(Serializable id) {
        System.out.println(baseDao instanceof UserDao);
        return (T) baseDao.get(id);
    }

    @Override
    public PageBean query(Map map) {
        PageMap pageMap=new PageMap(map);
        //查询第pageNum页  每页显示pageSize
        PageHelper.startPage(pageMap.getPageNum(),pageMap.getPageSize());
        List list=baseDao.query(map);
        return new PageBean(list);
    }
}
