package com.by.common.business;

import com.by.common.bean.PageBean;

import java.util.Map;

public interface IService<T> extends Crud<T>{

    PageBean query(Map map);
}
