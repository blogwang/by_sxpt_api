package com.by.common.business;

import java.io.Serializable;

/**
 * 增删改查 基本功能
 */
public interface Crud<T> {

    /**
     * 保存功能
     * @param t  要保存的对象 可以是map或javabean
     * @return  保存结果  0  保存失败 1  保存成功
     */
    int save(T t);

    /**
     * 修改功能
     * @param t  要修改的对象 可以是map或javabean
     * @return  修改结果  0  修改失败 1  修改成功
     */
    int update(T t);

    /**
     * 删除
     * @param id 要删除的记录id
     * @return 删除结果  0 删除失败  1 删除成功
     */
    int delete(Serializable id);

    /**
     * 根据id查询详情
     * @param id  记录id
     * @return 查询的结果  map或Javabean
     */
    T  get(Serializable id);


}
