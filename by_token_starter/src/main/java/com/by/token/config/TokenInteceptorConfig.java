package com.by.token.config;

import com.by.token.TokenProperties;
import com.by.token.inteceptor.TokenInteceptor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

public class TokenInteceptorConfig implements WebMvcConfigurer {

    TokenInteceptor tokenInteceptor;

    TokenProperties properties;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(tokenInteceptor)
                .addPathPatterns(properties.getPathPatterns())
                .excludePathPatterns(properties.getExcludePathPatterns());
    }

    public TokenInteceptor getTokenInteceptor() {
        return tokenInteceptor;
    }

    public void setTokenInteceptor(TokenInteceptor tokenInteceptor) {
        this.tokenInteceptor = tokenInteceptor;
    }

    public TokenProperties getProperties() {
        return properties;
    }

    public void setProperties(TokenProperties properties) {
        this.properties = properties;
    }
}
