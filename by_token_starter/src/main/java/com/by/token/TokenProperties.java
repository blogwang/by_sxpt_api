package com.by.token;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@ConfigurationProperties(prefix = "by.token")
public class TokenProperties {

   private String iss;
    private String sub;
    private  long validateTime;  //token有效时间 单位s
    private String secret;  //秘钥
    private String pathPatterns;//拦截请求
    private String [] excludePathPatterns;//不拦截的参数


    public String getIss() {
        return iss;
    }

    public void setIss(String iss) {
        this.iss = iss;
    }

    public String getSub() {
        return sub;
    }

    public void setSub(String sub) {
        this.sub = sub;
    }

    public long getValidateTime() {
        return validateTime;
    }

    public void setValidateTime(long validateTime) {
        this.validateTime = validateTime;
    }

    public String getSecret() {
        if(null==secret||"".equals(secret))
            return "ghjkllakdsjflajdflkjsda";
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public String getPathPatterns() {
        if(null==pathPatterns||"".equals(pathPatterns)){
            return "/**"; //拦截所有请求
        }
        return pathPatterns;
    }

    public void setPathPatterns(String pathPatterns) {
        this.pathPatterns = pathPatterns;
    }

    public String[] getExcludePathPatterns() {
        if(null==excludePathPatterns){
            return new String[0];
        }
        return excludePathPatterns;
    }

    public void setExcludePathPatterns(String[] excludePathPatterns) {
        this.excludePathPatterns = excludePathPatterns;
    }
}
