package com.by.autoconfig;

import com.by.token.TokenContext;
import com.by.token.TokenProperties;
import com.by.token.config.TokenInteceptorConfig;
import com.by.token.inteceptor.TokenInteceptor;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
@EnableConfigurationProperties(TokenProperties.class)
@ConditionalOnProperty(
        prefix = "by.token",
        name = "enable",
        havingValue ="true",
        matchIfMissing = false
)
public class TokenAutoConfigure {

    TokenProperties properties;

    public TokenAutoConfigure(TokenProperties properties){
        this.properties=properties;
    }

    /**
     * 当配置类生效时 向ioc容器注册token
     * @return
     */
    @Bean
    public TokenContext tokenContext(){
       return new TokenContext(properties);
    }

    @Bean
    public TokenInteceptorConfig tokenInteceptorConfig(TokenContext tokenContext){
        TokenInteceptorConfig config=new TokenInteceptorConfig();
        config.setTokenInteceptor(new TokenInteceptor(tokenContext));
        config.setProperties(properties);
        return config;
    }


}
